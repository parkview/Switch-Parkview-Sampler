EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Parkview Switch Sampler"
Date "2021-05-02"
Rev "1.1"
Comp "PRL"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW3
U 1 1 5EB827AD
P 3930 2315
F 0 "SW3" H 3930 2600 50  0001 C CNN
F 1 "SW_Push" H 3930 2509 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:SMD_Switch_5x3" H 3930 2515 50  0001 C CNN
F 3 "~" H 3930 2515 50  0001 C CNN
	1    3930 2315
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5EB847E8
P 3935 2795
F 0 "SW5" H 3935 3080 50  0001 C CNN
F 1 "SW_Push" H 3935 2989 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPST_8x3.6" H 3935 2995 50  0001 C CNN
F 3 "~" H 3935 2995 50  0001 C CNN
	1    3935 2795
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW15
U 1 1 5EB8F103
P 3930 5460
F 0 "SW15" H 3930 5745 50  0001 C CNN
F 1 "SW_SPDT" H 3930 5654 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPDT_Slide_9x6.9-hole" H 3930 5460 50  0001 C CNN
F 3 "~" H 3930 5460 50  0001 C CNN
	1    3930 5460
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW17
U 1 1 5EB8F42A
P 3870 6055
F 0 "SW17" H 3870 6340 50  0001 C CNN
F 1 "SW_SPDT" H 3870 6249 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:SMD_Switch_SPDT_PCM12_7.8x3.8" H 3870 6055 50  0001 C CNN
F 3 "~" H 3870 6055 50  0001 C CNN
	1    3870 6055
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5EB94C1D
P 4720 2350
F 0 "SW4" H 4720 2635 50  0001 C CNN
F 1 "SW_Push" H 4720 2544 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:SMD_Switch_5x3" H 4720 2550 50  0001 C CNN
F 3 "~" H 4720 2550 50  0001 C CNN
	1    4720 2350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5EB94C27
P 4725 2830
F 0 "SW6" H 4725 3115 50  0001 C CNN
F 1 "SW_Push" H 4725 3024 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPST_8x3.6" H 4725 3030 50  0001 C CNN
F 3 "~" H 4725 3030 50  0001 C CNN
	1    4725 2830
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW16
U 1 1 5EB94C31
P 4720 5495
F 0 "SW16" H 4720 5780 50  0001 C CNN
F 1 "SW_SPDT" H 4720 5689 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPDT_Slide_9x6.9-hole" H 4720 5495 50  0001 C CNN
F 3 "~" H 4720 5495 50  0001 C CNN
	1    4720 5495
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW18
U 1 1 5EB94C3B
P 4660 6090
F 0 "SW18" H 4660 6375 50  0001 C CNN
F 1 "SW_SPDT" H 4660 6284 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:SMD_Switch_SPDT_PCM12_7.8x3.8" H 4660 6090 50  0001 C CNN
F 3 "~" H 4660 6090 50  0001 C CNN
	1    4660 6090
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW19
U 1 1 608F46D4
P 3820 6745
F 0 "SW19" H 3820 7030 50  0001 C CNN
F 1 "SW_SPDT" H 3820 6939 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPDT_Top_Slide_7.8x3.8" H 3820 6745 50  0001 C CNN
F 3 "~" H 3820 6745 50  0001 C CNN
	1    3820 6745
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW20
U 1 1 608F4DC2
P 4630 6710
F 0 "SW20" H 4630 6995 50  0001 C CNN
F 1 "SW_SPDT" H 4630 6904 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPDT_Top_Slide_7.8x3.8" H 4630 6710 50  0001 C CNN
F 3 "~" H 4630 6710 50  0001 C CNN
	1    4630 6710
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 609073D3
P 3920 1700
F 0 "SW1" H 3920 1985 50  0001 C CNN
F 1 "SW_Push" H 3920 1894 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:SW-SMD-SPST-3.5x2.7" H 3920 1900 50  0001 C CNN
F 3 "~" H 3920 1900 50  0001 C CNN
	1    3920 1700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 60907587
P 4710 1735
F 0 "SW2" H 4710 2020 50  0001 C CNN
F 1 "SW_Push" H 4710 1929 50  0000 C CNN
F 2 "Pauls_KiCAD_Libraries:SW-SMD-SPST-3.5x2.7" H 4710 1935 50  0001 C CNN
F 3 "~" H 4710 1935 50  0001 C CNN
	1    4710 1735
	1    0    0    -1  
$EndComp
Text Notes 3250 1635 0    50   ~ 0
SW1
Text Notes 3245 2180 0    50   ~ 0
SW2
Text Notes 3250 2695 0    50   ~ 0
SW3
Text Notes 3230 3310 0    50   ~ 0
SW4
Text Notes 3225 3920 0    50   ~ 0
SW5
Text Notes 3225 4420 0    50   ~ 0
SW6
Text Notes 3215 4910 0    50   ~ 0
SW7
Text Notes 3215 5315 0    50   ~ 0
SW8
Text Notes 3220 5905 0    50   ~ 0
SW9
Text Notes 3210 6575 0    50   ~ 0
SW10
Text Notes 3780 1080 0    50   ~ 0
Footprint
Text Notes 4575 1060 0    50   ~ 0
Switch
NoConn ~ 4120 1700
NoConn ~ 3720 1700
NoConn ~ 4510 1735
NoConn ~ 4910 1735
NoConn ~ 4920 2350
NoConn ~ 4520 2350
NoConn ~ 4130 2315
NoConn ~ 3730 2315
NoConn ~ 3735 2795
NoConn ~ 4135 2795
NoConn ~ 4525 2830
NoConn ~ 4925 2830
NoConn ~ 4920 4345
NoConn ~ 4520 4345
NoConn ~ 3700 4325
NoConn ~ 4100 4325
NoConn ~ 3620 6745
NoConn ~ 4020 6845
NoConn ~ 4020 6645
NoConn ~ 4430 6710
NoConn ~ 4830 6810
NoConn ~ 4830 6610
NoConn ~ 4860 6190
NoConn ~ 4860 5990
NoConn ~ 4460 6090
NoConn ~ 4070 6155
NoConn ~ 4070 5955
NoConn ~ 3670 6055
NoConn ~ 3730 5460
NoConn ~ 4130 5560
NoConn ~ 4130 5360
NoConn ~ 4520 5495
NoConn ~ 4920 5395
NoConn ~ 4920 5595
$Comp
L Pauls_Symbol_Library:SW_SPST-4-pin SW13
U 1 1 6090EB1F
P 3950 4855
F 0 "SW13" H 3950 5140 50  0001 C CNN
F 1 "SW_SPST-4-pin" H 3950 5048 50  0000 C CNN
F 2 "SW_Footprints:SW_TH_PUSH_6mm_H4.3mm" H 3950 5055 50  0001 C CNN
F 3 "" H 3950 5055 50  0001 C CNN
	1    3950 4855
	1    0    0    -1  
$EndComp
$Comp
L Pauls_Symbol_Library:SW_SPST-4-pin SW14
U 1 1 6090F38E
P 4770 4875
F 0 "SW14" H 4770 5160 50  0001 C CNN
F 1 "SW_SPST-4-pin" H 4770 5068 50  0000 C CNN
F 2 "SW_Footprints:SW_TH_PUSH_6mm_H4.3mm" H 4770 5075 50  0001 C CNN
F 3 "" H 4770 5075 50  0001 C CNN
	1    4770 4875
	1    0    0    -1  
$EndComp
$Comp
L Pauls_Symbol_Library:SW_SPST-4-pin SW11
U 1 1 609108AB
P 3920 3245
F 0 "SW11" H 3920 3530 50  0001 C CNN
F 1 "SW_SPST-4-pin" H 3920 3438 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPST_4x3.3" H 3920 3445 50  0001 C CNN
F 3 "" H 3920 3445 50  0001 C CNN
	1    3920 3245
	1    0    0    -1  
$EndComp
$Comp
L Pauls_Symbol_Library:SW_SPST-4-pin SW12
U 1 1 609108CF
P 4740 3265
F 0 "SW12" H 4740 3550 50  0001 C CNN
F 1 "SW_SPST-4-pin" H 4740 3458 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPST_4x3.3" H 4740 3465 50  0001 C CNN
F 3 "" H 4740 3465 50  0001 C CNN
	1    4740 3265
	1    0    0    -1  
$EndComp
$Comp
L Pauls_Symbol_Library:SW_SPST-4-pin SW9
U 1 1 60911E55
P 3900 3825
F 0 "SW9" H 3900 4110 50  0001 C CNN
F 1 "SW_SPST-4-pin" H 3900 4018 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPST_Side_6.3x6.7" H 3900 4025 50  0001 C CNN
F 3 "" H 3900 4025 50  0001 C CNN
	1    3900 3825
	1    0    0    -1  
$EndComp
$Comp
L Pauls_Symbol_Library:SW_SPST-4-pin SW10
U 1 1 60912005
P 4720 3845
F 0 "SW10" H 4720 4130 50  0001 C CNN
F 1 "SW_SPST-4-pin" H 4720 4038 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPST_Side_6.3x6.7" H 4720 4045 50  0001 C CNN
F 3 "" H 4720 4045 50  0001 C CNN
	1    4720 3845
	1    0    0    -1  
$EndComp
NoConn ~ 4520 4445
NoConn ~ 4100 4425
NoConn ~ 3700 4425
NoConn ~ 3700 3825
NoConn ~ 3700 3925
NoConn ~ 4100 3925
NoConn ~ 4100 3825
NoConn ~ 4520 3845
NoConn ~ 4520 3945
NoConn ~ 4920 3845
NoConn ~ 4920 3945
NoConn ~ 4920 4445
NoConn ~ 4940 3265
NoConn ~ 4940 3365
NoConn ~ 4540 3265
NoConn ~ 4540 3365
NoConn ~ 4120 3345
NoConn ~ 4120 3245
NoConn ~ 3720 3245
NoConn ~ 3720 3345
NoConn ~ 3750 4955
NoConn ~ 3750 4855
NoConn ~ 4150 4855
NoConn ~ 4150 4955
NoConn ~ 4570 4975
NoConn ~ 4570 4875
NoConn ~ 4970 4875
NoConn ~ 4970 4975
$Comp
L Pauls_Symbol_Library:SW_SPST-4-pin SW7
U 1 1 60912D37
P 3900 4325
F 0 "SW7" H 3900 4610 50  0001 C CNN
F 1 "SW_SPST-4-pin" H 3900 4518 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPST_SKQG_WithStem_6x6.5" H 3900 4525 50  0001 C CNN
F 3 "" H 3900 4525 50  0001 C CNN
	1    3900 4325
	1    0    0    -1  
$EndComp
$Comp
L Pauls_Symbol_Library:SW_SPST-4-pin SW8
U 1 1 60912F23
P 4720 4345
F 0 "SW8" H 4720 4630 50  0001 C CNN
F 1 "SW_SPST-4-pin" H 4720 4538 50  0000 C CNN
F 2 "SW_Footprints:SW_SMD_SPST_SKQG_WithStem_6x6.5" H 4720 4545 50  0001 C CNN
F 3 "" H 4720 4545 50  0001 C CNN
	1    4720 4345
	1    0    0    -1  
$EndComp
$EndSCHEMATC
