# Project Name:  Parkview Switch Sampler  
*Project Started:  2020-03-28*  

This is a sample KiCAD PCB of all my tactile and slide switches that I have used in my projects.  The PCB lists all the KiCAD information regarding each switch, along with a sample switch and footprint.  

Note: some of these switch's will be made by multiple manufactures.  I selected the LCSC part with the highest stock level as of May 2021.  I haven't as yet, bothered to look up a comparable Octopart, or Digikey part number - if they exist.  
  
  
**PCB versions**  
  
*Version 1.1:*  
Production: 2021-05-??, JLCPCB, White, 1.6mm, 2 layer  
  
Done: create Gitlab project to host all the info  
Done: add GITLAB project address to the bottom back of PCB  
Done: collate switch PDF's into a GITLAB Doc folder  
Done: move name and version number to the top of the backside  
Done: re-order by size of switch:  SW1 = 55; SW2 = SW1;   
Done: look at streamlining KICAD foot print names  
Done: create full set of symbols  
Done: create full set of 3D STEP models  
Done: include LCSC part numbers  
Done: compile LCSC based switch datasheets into one folder  
Done: Add switch pin annotations for slider switches [1, 2, C or 1, c, 2]  
Done: is there room to add V & A switch ratings?  
Done: Add manufacture and MPN  
Done: Move SW # to LH side of switch and make it a bit larger and bolder text  
  
  
*Version 1.0:*  
Production: 2020-03-28, JLCPCB, White, 1.6mm  
Done:  includes all my momentory push button and tri-pole (9) switches  
  
